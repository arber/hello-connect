(defproject hello-connect "0.1.0-SNAPSHOT"
  :description "A demo Connect Addon"
  :url "https://bitbucket.org/ssmith/hello-connect/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :plugins [[lein-ancient "0.6.8"]
            [lein-cljsbuild "1.1.1"]]

  :hooks [leiningen.cljsbuild]

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.48"]

                 [clj-connect "0.2.1"]

                 [ring "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-json "0.4.0"]
                 [ring.middleware.logger "0.5.0"]
                 [compojure "1.4.0"]

                 [org.immutant/web "2.1.1"]

                 [clj-http "2.0.0"]

                 [environ "1.0.1"]

                 [selmer "0.9.5"]]

  :source-paths ["src" "src/clojure"]
  :test-paths ["test" "test/clojure"]
  :resource-paths ["resources"
                   "target/cljs"]

  :cljsbuild {:builds
              [{:source-paths ["src/cljs"]
                :compiler {:output-to "target/cljs/public/cljs/core.js"
                           :externs ["AP-externs.js"]
                           :optimizations :advanced}}]}

  :main ^:skip-aot hello-connect.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
