;; Bitbucket Connect also supports a client side library - AP (for "Atlassian Plugins") - that
;; allows you to interact with the host application. For example, you can make authenticated
;; requests to the Bitbucket REST API ...

(ns hello_connect.core
  (:require [cljs.reader :as reader]
            [goog.dom :as dom]
            [goog.style :as style]))

(enable-console-print!)

(defn set-name [data]
  (let [name (((js->clj data) "user") "display_name")]

    (-> (dom/getElement "displayName")
        (dom/setTextContent name))
    (-> (dom/getElement "nloading")
        (style/setElementShown false))))

(.require js/AP "request"
          (fn [request]
            (request (clj->js
                      {"url" "/1.0/user/"
                       "success" set-name}))))

;; ... and set cookies (browser security policies often prevent this from being done in iframes).

(def cookie-name "example-visits")

(defn set-count [count]
  (-> (dom/getElement "pageVisits")
      (dom/setTextContent count))
  (-> (dom/getElement "cloading")
      (style/setElementShown false)))

(.require js/AP "cookie"
          (fn [cookie]
            (.read cookie cookie-name (fn [visits]
                                        (let [n (inc (reader/read-string visits))]
                                          (.save cookie cookie-name n 30)
                                          (set-count n))))))
