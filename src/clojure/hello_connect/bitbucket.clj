(ns hello-connect.bitbucket
  (:require [clojure.tools.logging :as log]

            [clj-connect.jwt :as jwt]
            [clj-http.client :as http]
            [environ.core :refer [env]]
            [hello-connect.storage :as storage]))


(defn fetch-oauth-token []
  (let [token (jwt/gen-jwt-token "POST"
                                 "/site/oauth2/access_token"
                                 {}
                                 (env :project-key)
                                 (storage/client-key)
                                 (storage/shared-secret))

        resp (http/post "https://bitbucket.org/site/oauth2/access_token"
                        {:headers {"Authorization" (str "JWT " token)}
                         :form-params {:grant_type "urn:bitbucket:oauth2:jwt"}
                         :as :json})]

    (-> resp
        :body
        :access_token)))


(defn bb-display-name [oauth]

  (let [data (http/get "https://bitbucket.org/api/1.0/user/" {:oauth-token oauth
                                                                :as :json})]
    (log/info "Received user data" data)
    (-> data
        :body
        :user
        :display_name)))

