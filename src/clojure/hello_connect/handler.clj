(ns hello-connect.handler
  (:require [clojure.tools.logging :as log]

            [ring.util.response :as response]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.json :refer [wrap-json-body]]

            [clj-connect.jwt :as jwt]

            [environ.core :refer [env]]
            [selmer.parser :as selmer]

            [hello-connect.storage :as storage]
            [hello-connect.bitbucket :as bitbucket]))


(defn wrap-jwt-auth [request handler]
  (if (not (jwt/verify-jwt request (storage/shared-secret)))
    {:status 401}
    (handler request)))


(defn gen-descriptor []
  ;; Fetch configuration from the environment (see `environ` docs)
  (let [ctx {:base-url (env :base-url)
             :oauth-key (env :oauth-key)}]
    (selmer/render-file "views/atlassian-connect.json.selmer" ctx)))

(defn gen-descriptor-reply []
  (log/info "Received descriptor request")
  {:status 200
   :headers {"Content-Type" "application/json; charset=utf-8"}
   :body (gen-descriptor)})


(defn process-installed [params body]
  (log/info "Received /installed")
  (storage/save-addon-context body)
  {:status 204})

(defn process-uninstalled [request]
  (log/info "Received /uninstalled")
  (storage/delete-addon-context)
  {:status 204})

(defn process-webhook [request]
  (log/info "Received /webhook of:\n" (clojure.pprint/pprint (request :body)))  
  {:status 204})

(defn get-repo-page [request]
  (let [{jwt-token "jwt"
         repo-path "repoPath"} (request :query-params)

         oauth (bitbucket/fetch-oauth-token)
         ctx {:repopath repo-path
              :displayname (bitbucket/bb-display-name oauth)}]
    (log/info "Getting repo information for " repo-path)

    {:status 200
     :headers {"Content-Type" "text/html; charset=utf-8"}
     :body (selmer/render-file "views/connect-example.selmer" ctx)}))


(defroutes app-routes
  (GET  "/" [] (response/redirect "/atlassian-connect.json"))
  (GET  "/atlassian-connect.json" []
        (gen-descriptor-reply))

  (POST "/installed" {params :query-params body :body}
        (process-installed params body))

  (POST "/uninstalled" request
        (wrap-jwt-auth request process-uninstalled))

  (POST "/webhook" request
        (wrap-jwt-auth request process-webhook))

  (GET  "/connect-example" request
        (wrap-jwt-auth request get-repo-page))
  
  (route/not-found {:status 404 :body "Not Found"}))


(def app
  ;; Disable anti-forgery as it interferes with Connect POSTs
  (let [connect-defaults (-> site-defaults
                             (assoc-in [:security :anti-forgery] false)
                             (assoc-in [:security :frame-options] false)) ]

    (-> app-routes
        (wrap-defaults connect-defaults)
        (wrap-json-body))))

(defn init []
  (log/info "Initialising application")
    (storage/load-addon-context))
1
(defn shutdown []
  (log/info "Shutting down application"))
