(ns hello-connect.storage
  (:require [clojure.string :as string]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [environ.core :refer [env]]))

(defonce addon-ctx (atom nil))
(defonce context-file (str (env :data-dir "/tmp") "/addon-context"))


(defn load-addon-context []
  ;; Load up shared key if we have one
  (when (.exists (io/as-file context-file))
    (reset! addon-ctx (edn/read-string (slurp context-file)))
    (log/info "Loaded addon context of" @addon-ctx)))

(defn save-addon-context [ctx]
  (reset! addon-ctx ctx)
  (io/delete-file context-file  {:silently true})
  (spit context-file ctx))

(defn delete-addon-context []
  (io/delete-file context-file {:silently true})
  (reset! addon-ctx nil))

(defn shared-secret []
  (@addon-ctx "sharedSecret"))

(defn client-key []
  (@addon-ctx "clientKey"))
